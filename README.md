# PATH-LOGGER

[![NPM Version][npm-image]][npm-url]
[![Node.js Version][node-version-image]][node-version-url]
[![NPM Downloads][downloads-image]][downloads-url]

__Table of contents__

- [PATH-LOGGER](#path-logger)
  - [Installation](#installation)
  - [Usage](#usage)

## Installation

Path-Logger can be installed on Linux, Mac OS or Windows without any issues.
Implements with Express.js

Install via NPM

```
npm install --save path-logger
```

## Usage
1. Import

```javascript
const express = require("express");
const pl = require('path-logger');
```

2. To log a message

```javascript
const app = express();
app.post("/some-path", upload.single("fileupload"), async (req, res) => {
  // some statement
  res.status(201).send({message: "Create resource successfully!!!"});
  pl.log(req, res, 'Create resource successfully!!!');
});

// Output Console
// [POST] .../some-path | [201] – Create resource successfully!!!
```


[downloads-image]: https://img.shields.io/packagecontrol/dt/path-logger
[downloads-url]: https://www.npmjs.com/package/path-logger
[npm-image]: https://img.shields.io/npm/v/path-logger
[npm-url]: https://www.npmjs.com/package/path-logger
[node-version-image]: https://img.shields.io/node/v/path-logger.svg
[node-version-url]: http://nodejs.org/download/
