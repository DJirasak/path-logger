/**
 *
 * @param {Number} httpCode status code
 * @param {String} meth Method
 * @param {String} path path
 * @param {String} msg message
 * @example
 * log(200, 'GET','/','Send hello world successfully!!!')
 * // [GET] .../ | [200] – Send hello world successfully!!!
 */
function log(req, res, msg = "") {
  headHttp = Number(String(res.statusCode)[0]) - 1;
  shade = ["\x1b[36m", "\x1b[32m", "\x1b[33m", "\x1b[35m", "\x1b[31m"];
  let fullMSG = msg == "" ? "" : ` – ${msg}`;
  let log = `[${req.method.toUpperCase()}] .${req.baseUrl}${req.path} | [${
    res.statusCode
  }]${fullMSG}`;
  console.log(shade[headHttp], log, "\x1b[0m");
}

module.exports = {
  log,
};
